### Hey there, I'm Aditya - aka AdityaKrCodes 👋
<!-- [![Website](https://img.shields.io/website?style=flat-square&url=https%3A%2F%2Fadityakrcodes.github.io%2F)
](https://adityakrcodes.github.io/) -->

<img src="https://komarev.com/ghpvc/?username=adityakrcodes&label=Profile%20views&color=0e75b6&style=flat-square" alt="adityakrcodes" /> </p> 
### A passionate frontend developer from India

 
  
- 🤓 I’m currently learning **BASH** ,**Python** & **JavaScript** 
  
- 💬 Ask me about **Web Development**  
  
<h3 align="left">Connect with me:</h3>  

[![Twitter Follow](https://img.shields.io/twitter/follow/adityakrcodes?color=black&label=%40adityakrcodes&logo=Twitter&style=flat-square)](https://www.twitter.com/adityakrcodes)
[![Instagram Follow](https://img.shields.io/badge/IG:-@adityakrcodes-black?style=flat-square&logo=instagram)](https://www.instagram.com/adityakrcodes)
<p align="left">  
  
<h3 align="left">Languages and Tools:</h3>  
<p align="left"> <a href="https://www.gnu.org/software/bash/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt="bash" width="40" height="40"/> </a> <a href="https://www.w3schools.com/css/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://git-scm.com/" target="_blank"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> <a href="https://www.linux.org/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/linux/linux-original.svg" alt="linux" width="40" height="40"/> </a> <a href="https://www.python.org" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/python/python-original.svg" alt="python" width="40" height="40"/> </a> </p>  
<p><img align="center" src="https://github-readme-stats.vercel.app/api/top-langs?username=adityakrcodes&show_icons=true&locale=en&layout=compact" alt="adityakrcodes" /></p>
<h3 align="left"> 🎧 What am I listening: <h3>

[![Spotify](https://github-spotify-now-playin.vercel.app/api/spotify)](https://open.spotify.com/user/31ke65y4vvmlqqqsjybg3foclmw4)

# My Daily.dev Card
<a href="https://app.daily.dev/adityakrcodes"><img src="https://api.daily.dev/devcards/6d125c10383a4d71aee776ab78253370.png?r=cp3" width="400" alt="Aditya Kumar's Dev Card"/></a>

<h3 align="left">Support:</h3>  
<p><a href="https://www.buymeacoffee.com/adityakrcodes"> <img align="left" src="https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png" height="50" width="210" alt="adityakrcodes" /></a></p><br><br>  
 
